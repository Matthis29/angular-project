import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Ma super app';
  jour: number = 12;
  mois: number = 11;
  annee: number = 2018;
  items: string[] = ['Home', 'Articles', 'Ajout-article', 'Contact'];
}
