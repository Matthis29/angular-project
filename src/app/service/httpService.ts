import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Article} from '../model/Article';
import {map, tap} from 'rxjs/operators';

@Injectable()
export class HttpService {
    constructor(private http: HttpClient) {}

    public getAllArticles(): Observable<Article[]> {
        return this.http
            .get('http://localhost:4200/backAngular')
            .pipe(
                map((jsonArray: Object[]) => jsonArray.map( jsonItem => Article.fromJson(jsonItem)))
            );
    }

    public getArticleById($id): Observable<Article> {
        return this.http
            .get('http://localhost:4200/backAngular?id=' + $id)
            .pipe(
                tap((value) => { console.log('RESPONSE HTTP -> ', value); }),
                map((jsonItem) => {
                     return Article.fromJson(jsonItem);
                })
            );
    }

    public putArticles(article) {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        this.http
            .put('http://localhost:4200/backAngular', article, {headers})
            .subscribe((val) => {
                console.log(val);
            });
    }

}
