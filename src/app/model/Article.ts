export class Article {
    id: number;
    titre: string;
    auteur: string;
    contenu: string;


    constructor(id: number, titre: string, auteur: string, contenu: string) {
        this.id = id;
        this.titre = titre;
        this.auteur = auteur;
        this.contenu = contenu;
    }

    public static fromJson(json: Object): Article {
        return new Article(
            json['id'],
            json['titre'],
            json['auteur'],
            json['contenu']
        );
    }
}