import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, ParamMap} from '@angular/router';
import {from, interval, Observable, of} from 'rxjs';
import {Article} from '../model/Article';
import {HttpService} from '../service/httpService';

@Component({
  selector: 'app-article-detail',
  templateUrl: './article-detail.component.html',
  styleUrls: ['./article-detail.component.css']
})
export class ArticleDetailComponent implements OnInit {
  id: any;
  article: Article;

  constructor(private route: ActivatedRoute, private httpService: HttpService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.id = params.get("id");
    });

    this.httpService.getArticleById(this.id).subscribe((response) => {
      this.article = response;
    });

      /*this.route.queryParamMap.subscribe((params) => {
          this.article = new Article(this.id, params.get("titre"), params.get("auteur"), params.get("contenu"));
      });*/

    /*const myObservable = of(95);
    myObservable.subscribe((value) => {
      console.log("APRES => ", value);
    }, (error) => {
      console.log(error);
    }, () => {
      console.log("FINI");
    });

    const arrayObservable = from(["hello", "world"]);
    arrayObservable.subscribe((toto) => {
      console.log(toto);
    });

    const intervableObservable = interval(2000);
    intervableObservable.subscribe((compteur) => {
      console.log(compteur);
    })*/

  }

}
