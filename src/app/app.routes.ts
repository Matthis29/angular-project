import {Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {ArticlesComponent} from './articles/articles.component';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {ArticleDetailComponent} from './article-detail/article-detail.component';
import {ArticleListComponent} from './article-list/article-list.component';
import {ArticleAjoutComponent} from './article-ajout/article-ajout.component';

export const ROUTES: Routes = [
    { path: '', redirectTo: '/Home', pathMatch: 'full'},
    { path: 'Home', component: HomeComponent},
    {path: 'Ajout-article', component: ArticleAjoutComponent},
    {
        path: 'Articles',
        component: ArticlesComponent,
        children: [
            {path: '', pathMatch: 'full', redirectTo: 'list'},
            {path: 'list', component: ArticleListComponent},
            {path: ':id', component: ArticleDetailComponent},
        ]
    },
    { path: '**', component: PageNotFoundComponent}
];