import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Article} from '../model/Article';
import {HttpService} from '../service/httpService';

@Component({
  selector: 'app-article-ajout',
  templateUrl: './article-ajout.component.html',
  styleUrls: ['./article-ajout.component.css']
})
export class ArticleAjoutComponent implements OnInit {
  form: FormGroup;

  constructor(private fb: FormBuilder, private httpService: HttpService) {
    this.form = fb.group({
        'titre': [''],
        'auteur': [''],
        'contenu': ['']
    });
  }

  ngOnInit() {
  }

  public addArticle($value){
      console.log($value);
      if($value.titre !== '' && $value.auteur !== "" && $value.contenu !== ""){
          const article = new Article(null, $value.titre, $value.auteur, $value.contenu);
          this.httpService.putArticles(article);

      }
  }

}
