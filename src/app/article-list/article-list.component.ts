import { Component, OnInit } from '@angular/core';
import {Article} from '../model/Article';
import {HttpService} from '../service/httpService';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {
    listArticles: Array<Article>;
    interval: any;

    /*listArticles: Array<Article> = [
        {id: 1, titre: "titre 1", auteur: "auteur 1", contenu: "Contenu 1"},
        {id: 2, titre: "titre 2", auteur: "auteur 2", contenu: "Contenu 2"},
        {id: 3, titre: "titre 3", auteur: "auteur 3", contenu: "Contenu 3"}
    ]*/

  constructor(private httpService: HttpService) { }

  ngOnInit() {
      /*this.interval = setInterval(() => {
          this.httpService.getAllArticles().subscribe((response) => {
              this.listArticles = response;
          });
      }, 1200);*/

      this.httpService.getAllArticles().subscribe((response) => {
          this.listArticles = response;
      });
  }

  getArticleById($id){
        /*const element = this.listArticles.find(x => x.id == $id);
        return element;*/
        return this.listArticles.find(value => value.id == $id);
    }

}
